import { OAuth2Client } from 'google-auth-library';
import { google } from 'googleapis';
import conf from './config';
import { Context } from 'koa';

export class GoogleCredential {

  private static _client?: OAuth2Client;
  private static _inited = false;

  private token: any;
  private user: any;

  constructor(private ctx: Context) {
    this.token = ctx.session!.token;
    this.user = ctx.session!.user;
  }

  public getCredential() {
    const { ctx, ctx: { session } } = this;
    const client = GoogleCredential.create(ctx.host);

    client.setCredentials(session!.token);

    return client;
  }

  public getOAuth2Client() {
    return GoogleCredential._client;
  }

  public static reset(ctx: Context) {
    ctx.session && delete ctx.session.token;

    this._client = undefined;
    this._inited = false;
  }

  /**
   * 初始化 google credential。
   * @param arg 初始化參數。
   */
  public static async init(arg: Context | OAuth2Client) {

    if (this._inited) return;

    if (arg instanceof OAuth2Client) {
      this.setOAuth2Client(arg);
    } else {
      return GoogleCredential.initFromContext(arg);
    }
  }

  /**
   * 建立一個新的 OAuth2Client 物件。
   */
  public static create(host: string) {
    const { client_secret, client_id, redirect_uris } = conf.google_secret;
    let redirect_uri = undefined;

    for (const url of redirect_uris) {
      if (url.includes(host)) {
        redirect_uri = url;
      }
    }

    if (!redirect_uri) throw new Error(`redirect host not allowed： ${host}`);

    return new google.auth.OAuth2(client_id, client_secret, redirect_uri);
  }

  /**
   * 從 Context 讀取相關 session 資料進行初始化(ctx.session.token)。
   * @param ctx request context.
   */
  private static initFromContext(ctx: Context) {
    if (!ctx.session) throw new Error("Session not available.");

    const oAuth2Client = this.create(ctx.host);

    if (!ctx.session.token) throw new Error("您還未登入 Google。");

    // access_token, expiry_date, refresh_token, scope, token_type
    const { refresh_token } = ctx.session.token;

    return new Promise<void>(async (resolve, reject) => {
      oAuth2Client.setCredentials({ refresh_token: refresh_token });
      oAuth2Client.refreshAccessToken(async (err, creden, rsp) => {
        if (err) {
          reject(err);
        }
        else {
          oAuth2Client.setCredentials(creden!);
          this.setOAuth2Client(oAuth2Client);
          resolve();
        }
      });
    });
  }

  /**
   * 指定 OAuth2Client 物件，並同步相關變數狀態。
   */
  private static setOAuth2Client(client: OAuth2Client) {
    this._client = client;
    this._inited = true;
  }
}

declare module 'koa' {

  interface Context {
    /**
     * Google Credentials。
     */
    google: OAuth2Client;
  }
}
