import http from 'http';
import path from 'path';
import Koa from 'koa';
import serve from 'koa-static';
import send from 'koa-send';
import KoaSession from 'koa-session';
import sessionStore from './session_store';
import bodyParser from 'koa-bodyparser';

import { API } from '@ecoboost/core';
import { DefaultDatabaseToken, connection } from './database';

const PORT = process.env.PORT || 3535;

const session_conf = {
    key: '@ecoboost-pmserver', /** (string) cookie key (default is koa:sess) */
    maxAge: 864000000, // 用戶端 cookie 過期時間，多久沒使用 cookie 會失效。
    overwrite: true, /** (boolean) can overwrite or not (default true) */
    httpOnly: true, /** (boolean) httpOnly or not (default true) */
    signed: true, /** (boolean) signed or not (default true) */
    rolling: true, /** (boolean) Force a session identifier cookie to be set on every response. The expiration is reset to the original maxAge, resetting the expiration countdown. (default is false) */
    renew: true, /** (boolean) renew session when session is nearly expired, so we can always keep user logged in. (default is false)*/
    store: sessionStore
  };

async function main(app: Koa) {

    const api = await API.load(path.join(__dirname, 'service'), {
        providers: [
            // { provide: GoogleCredentialToken, useClass: GoogleCredential },
            { provide: DefaultDatabaseToken, useValue: connection }
        ]
    });

    app.keys = ['@ecoboost-realsavior-key']; // 不設定 koa-session 不運作。

    app.use(KoaSession(session_conf, app));
    app.use(serve("./public")); // 靜態檔案。
    
    app.use(bodyParser());
    // service router。
    app.use(api.routes("/api"));

    app.use(async (ctx) => {
        await send(ctx, "./public/index.html"); // html5 mode
    });

    http.createServer(app.callback()).listen(PORT);
    console.log('pmweb server startd!');

}

main(new Koa());

