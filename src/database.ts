/**
 * 資料庫相關初始設定。
 * 
 * Library：https://github.com/vitaly-t/pg-promise
 * TypeScript Ext Document：https://github.com/vitaly-t/pg-promise/tree/master/typescript
 */

import PGP from 'pg-promise';
import conf from './config';
import { Context } from 'koa';

const pgp = PGP();

/** 資料庫操作介面 */
export interface PGConnection extends PGP.IDatabase<any> {
}

/** 預設資料庫連線 Token。 */
export const DefaultDatabaseToken = Symbol("default database");

/** 資料庫連線 instance。 */
export const connection = pgp(conf.connectionString);

export function prepareDatabase(ctx: Context, next: any) {
    ctx.db = ctx.injector.get<PGConnection>(DefaultDatabaseToken);
    return next();
}

declare module 'koa' {
    interface Context {
        /**
         * 預設資料庫連線。
         */
        db: PGConnection;
    }
}