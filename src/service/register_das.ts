import { Package, Service, ServiceMethod } from "@ecoboost/core";
import { LoginVerifies } from './session';
import { prepareDatabase } from '../database';
import { Context } from 'koa';

@Package({middleware: [...LoginVerifies, prepareDatabase]})
export class RegisterDSA {

    @Service({ method: ServiceMethod.Post })
    public async set(ctx: Context, next: any) {
        const { id: savior_id } = ctx.user;
        const { dsns, login_user, login_password, name, description } = ctx.request.body as any;

        const cmd = `
            with insert as (
                insert into register_dsa(ref_savior_id, dsns, login_user, login_password, name , description)
                values($(savior_id), $(dsns), $(login_user), $(login_password), $(name), $(description))
                on conflict on constraint register_dsa_uniq do nothing
                returning id
            ), update as (
                update register_dsa
                set login_user = $(login_user), login_password = $(login_password), name = $(name), description = $(description)
                where ref_savior_id = $(savior_id) and dsns = $(dsns)
                returning id
            )
            select * from insert
            union all
            select * from update
        `;
        
        try {
            const result = await ctx.db.one(cmd, {
                savior_id, dsns, login_user, login_password, name, description
            });
    
            ctx.body = result;
        } catch(error) {
            ctx.status = 500;
            ctx.body = error.message;
        }

    }

    @Service({ method: ServiceMethod.Post })
    public async delete(ctx: Context) {
        const { id: savior_id } = ctx.user;
        const { id, dsns } = ctx.request.body as any;

        const conditions = [
            'ref_savior_id = $(savior_id)',
        ];

        if (id) conditions.push('id = $(id)');
        if (dsns) conditions.push('dsns = $(dsns)');

        const cmd = `
            delete from register_dsa 
            where ${conditions.length > 1 ? conditions.join(' and ') : false}
            returning id;
        `;

        try {
            const result = await ctx.db.oneOrNone(cmd, {
                savior_id, dsns, id
            });

            ctx.body = result;
        } catch (error) {
            ctx.status = 500;
            ctx.body = error.message;
        }
    }

    @Service()
    public async list(ctx: Context) {
        const { id: savior_id } = ctx.user;

        const cmd = 'select id, dsns, name, description from register_dsa where ref_savior_id = $(savior_id)';

        try {
            const records = await ctx.db.manyOrNone(cmd, { savior_id });
            ctx.body = records;

        } catch (error) {
            ctx.status = 500;
            ctx.body = error.message;
        }
    }

    @Service({ method: ServiceMethod.Post })
    public async get(ctx: Context) {
        const { id: savior_id } = ctx.user;
        const { id, dsns } = ctx.request.body as any;

        const conditions = [
            'ref_savior_id = $(savior_id)',
        ];

        if (id) conditions.push('id = $(id)');
        if (dsns) conditions.push('dsns = $(dsns)');

        const cmd = `select * from register_dsa 
            where ${conditions.length > 1 ? conditions.join(' and ') : false}`;

        try {
            const records = await ctx.db.one(cmd, { savior_id, id, dsns });
            ctx.body = records;

        } catch (error) {
            ctx.status = 500;
            ctx.body = error.message;
        }
    }
}