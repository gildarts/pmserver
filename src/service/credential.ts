import { Package, Service, ServiceMethod } from '@ecoboost/core';
import { PGConnection, prepareDatabase } from '../database';
import { LoginVerifies } from './session';
import { Context } from 'koa';

@Package({ middleware: [...LoginVerifies, prepareDatabase] })
export class Credential {

    /**
     * 取得 credential 清單。
     */
    @Service({ method: ServiceMethod.Post })
    public async list(ctx: Context) {
        const { id } = ctx.session!.user;
        const { kind, usage, principal } = ctx.request.body as any;
        const db: PGConnection = ctx.db;

        const conditions: string[] = ['ref_savior_id = $(id)'];

        if (kind) conditions.push('kind = $(kind)');
        if (usage) conditions.push('usage = $(usage)');
        if (principal) conditions.push('principal = $(principal)');

        const cmd = `
            select *
            from credential
            -- 有條件才查詢資料，只有 ref_savior_id 不算條件。
            where ${conditions.length > 1 ? conditions.join(' and ') : false}
        `;

        const records = await db.manyOrNone(cmd, { id, kind, usage, principal });

        ctx.body = records.map(v => {
            return {
                ...v,
                ref_savior_id: undefined, // 去掉 ref_savior_id 欄位。
                content: undefined
            }
        });
    }

    /**
     * 取得單一 credential 完整資料。
     */
    @Service({ method: ServiceMethod.Post })
    public async get(ctx: Context) {
        const { id: savior_id } = ctx.session!.user;
        const { id } = ctx.request.body as any;
        const db: PGConnection = ctx.db;

        const conditions: string[] = ['id = $(id)', 'ref_savior_id = $(savior_id)'];

        const cmd = `
            select content
            from credential
            where ${conditions.length > 1 ? conditions.join(' and ') : false}
        `;

        const credential = await db.oneOrNone(cmd, { id, savior_id });
        const content = credential && credential.content;
        ctx.body = {
            ...content, ...{ refresh_token: undefined } // 把 refresh_token 去掉。
        };
    }

    /**
     * 儲存取得的 Credential。
     *
     * @static
     * @param {DBContext} ctx
     * @param {string} kind 分類，例如用在 contract、google_calendar...
     * @param {string} usage 實際使用方式，通常是 contract 名稱，也可保持 null。
     * @param {string} principal Credential 主體，通常是帳號名稱。
     * @param {*} content Credential 內容。
     * @returns
     * @memberof Credential
     */
    public static async saveCredential(ctx: Context, kind: string, usage: string, principal: string, provider: string, content: any) {

        const { id } = ctx.session!.user;
        const db: PGConnection = ctx.db;

        const cmd = `
        with update as (
            update credential
            set content=$(content:json)
            where ref_savior_id=$(ref_savior_id) and kind=$(kind) and usage=$(usage) and principal=$(principal)
            returning *
        ), 
        insert as (
            insert into credential(kind, usage, ref_savior_id, principal, provider, content)
            values($(kind), $(usage), $(ref_savior_id), $(principal), $(provider), $(content:json))
            on conflict on constraint credential_uniq do nothing
            returning *
        )
        select * from update
        union all
        select * from insert
        `;

        return db.one(cmd, {
            kind: kind,
            usage: usage,
            ref_savior_id: id,
            principal: principal,
            provider: provider,
            content: content
        });
    }
}