import { Package, Service, Injector } from '@ecoboost/core';
import { GoogleCredential } from '../google_credential';
import { prepareDatabase } from '../database';
import { Context } from 'koa';

@Package({ middleware: [Session.checkSessionAvailable] })
export class Session {

  constructor(injector: Injector) {
  }

  @Service({middleware: [prepareDatabase, Session.prepareGoogleCredential]})
  public async get(ctx: Context) {
    ctx.body = ctx.google.credentials;
  }

  @Service()
  public async clean(ctx: Context) {
    // GoogleCredential.reset(ctx);
    ctx.session = null;
    ctx.body = { success: true }
  }

  public static async prepareGoogleCredential(ctx: Context, next: any) {
    try {
      if(!ctx.session!.token) throw new Error("您還未登入 Google。");

      const gc = new GoogleCredential(ctx);
      ctx.google = gc.getCredential();

      return next();
    } catch(error) {
      ctx.status = 500;
      ctx.body = { success: false, message: error.message || error }
    }
  }

  /**
   * 檢查 session 機制是否在目前在應用程式中可用，否則丟出錯誤。
   */
  public static async checkSessionAvailable(ctx: Context, next: any) {
    if (!ctx.session) {
      ctx.status = 500;
      ctx.body = {success: false, message: "Session not available."};
    } else {
      return next();
    }
  }

  public static async prepareUserInfo(ctx: Context, next: any) {
    ctx.user = ctx.session!.user;
    return next();
  }
}

/**
 * 檢查登入狀態必要項目。
 */
export const LoginVerifies = [Session.checkSessionAvailable, Session.prepareGoogleCredential, Session.prepareUserInfo];
