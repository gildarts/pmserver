import { Package, Service } from '@ecoboost/core';
import { GoogleCredential } from '../google_credential';
import { Session } from './session';
import got from 'got';
import { prepareDatabase, PGConnection } from '../database';
import { Context } from 'koa';

/** google token 過期。
 * ya29.GlsBBtAeYt1vDOMy_Z48Rn0PL8DrM4PfkziuVucudhGZ3rBvXC35_t-nDoUQlceC01EQyPSUkMT2r_pAfAaTOi6a7jFfy18M9UULKbt1saLvKVscIeqg6yefeH9E
{
 "error": {
  "errors": [
   {
    "domain": "global",
    "reason": "authError",
    "message": "Invalid Credentials",
    "locationType": "header",
    "location": "Authorization"
   }
  ],
  "code": 401,
  "message": "Invalid Credentials"
 }
}
 */

@Package({ middleware: [Session.checkSessionAvailable, prepareDatabase] })
export class Auth {

  private SCOPES = [
    'https://www.googleapis.com/auth/calendar.readonly',
    'https://www.googleapis.com/auth/spreadsheets.readonly',
    'https://www.googleapis.com/auth/userinfo.profile',
    "https://www.googleapis.com/auth/userinfo.email"
  ];

  // 登入後頁面。
  private postLoginUrl = '/';

  constructor() {
  }

  @Service()
  public async authorize(ctx: Context) {
    try {
      const oAuth2Client = GoogleCredential.create(ctx.host);
  
      const authUrl = oAuth2Client.generateAuthUrl({
        access_type: 'offline', // 只有第一次取得 refresh_token
        prompt: "select_account", //　每次登入都會確認，有機會換帳號登入。
        scope: this.SCOPES,
      });
  
      ctx.session!.restoreUrl = ctx.query.restore_url;
      ctx.redirect(authUrl);
    } catch(error) {
      ctx.status = 500;
      ctx.body = error.message;
    }
  }

  @Service()
  public async callback(ctx: Context) {
    const oAuth2Client = GoogleCredential.create(ctx.host);

    const { code } = ctx.query;

    return new Promise<void>((resolve) => {
      oAuth2Client.getToken(code, async (err: any, token: any) => {

        if(err) {
          ctx.body = { code: err.code, message: err.message };
          resolve();
          return;
        }

        const user = await Auth.prepareUser(ctx.db, token);         

        ctx.session!.token = token; // 會寫入到資料庫。
        ctx.session!.user = user; // 會寫入到資料庫。

        // oAuth2Client.setCredentials(token);

        // GoogleCredential.init(oAuth2Client);

        if (!ctx.session!.restoreUrl) {
          ctx.redirect(this.postLoginUrl);
        } else {
          ctx.redirect(ctx.session!.restoreUrl);
        }

        resolve();
      });
    })
  }

  @Service({ middleware: Session.prepareGoogleCredential })
  public async available(ctx: Context) {
    ctx.body = { success: true };
  }

  @Service({ middleware: Session.prepareGoogleCredential })
  public async userInfo(ctx: Context) {
    const { user: { user_info } } = ctx.session!;
    ctx.body = user_info;
  }

  private static async prepareUser(db: PGConnection, token: any) {

    const { access_token, refresh_token } = token;
    const { body, body: { email } } = await Auth.getUserFromGoogle(access_token);
    let dbuser = await Auth.getUserFromDB(db, email);

    if (!dbuser) { // 使用者不存在時。
      dbuser = await Auth.createUser(db, email, body, refresh_token);
    } else { // 使用者存在，但又有 refresh_token 才更新。
      if (refresh_token) {
        dbuser = await Auth.updateUser(db, email, body, refresh_token);
      } else {
        token.refresh_token = dbuser.google_refresh_token;
      }
    }

    return dbuser;
  }

  private static async getUserFromGoogle(access_token: string) {
    const url = `https://www.googleapis.com/oauth2/v1/userinfo?alt=json&access_token=${access_token}`;
    return await got(url, { json: true });
  }

  private static async getUserFromDB(db: PGConnection, email: string) {
    const cmd = "select * from savior where account = $(account)";
    return db.oneOrNone(cmd, { account: email });
  }

  private static async createUser(db: PGConnection, email: string,  data: any, refresh_token: string) {
    const cmd = `insert into savior(account, google_refresh_token, user_info)
                 values($(account), $(refresh_token), $(user_info))
                 returning *`;
    return await db.one(cmd, { account: email, refresh_token: refresh_token, user_info: data });

  }

  private static async updateUser(db: PGConnection, email: string,  data: any, refresh_token: string) {
    const cmd = `update savior set google_refresh_token = $(refresh_token), user_info = $(user_info)
                 where account = $(account)
                 returning *`;
    return await db.one(cmd, { account: email, refresh_token: refresh_token, user_info: data });
  }
}

declare module 'koa' {
  
  interface Context {
    /**
     *使用者資訊。
     */
    user: { id: number, account: string };
  }
}
