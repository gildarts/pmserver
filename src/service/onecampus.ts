import { Package, Service } from '@ecoboost/core';
import { prepareDatabase } from '../database';
import { LoginVerifies } from './session';
import got from 'got';
import { Credential } from './credential';
import { Context } from 'koa';

@Package({ middleware: [...LoginVerifies, prepareDatabase] })
export class OneCampus {

    private static readonly Scopes = [
        'User.Mail',
        'User.BasicInfo',
    ];

    private static readonly provider = '1campus';
    private static readonly auth_service = 'https://auth.ischool.com.tw/oauth/authorize.php';
    private static readonly token_service = 'https://auth.ischool.com.tw/oauth/token.php';
    private static readonly me_service = 'https://auth.ischool.com.tw/services/me.php';
    private static readonly client_id = 'd44536a62827c9a396c50281c6e72454';
    private static readonly client_secret = '45eaddd3a19c41529a762c144e0d30b1c2fd2dd5045f04c877bd28f4e92683f3';

    // 登入後頁面。
    private static readonly postLoginUrl = '/ischool/callback';

    /**
     * 必要參數：kind、usage、scope
     */
    @Service()
    public async authorize(ctx: Context) {
        try {
            const { scope } = ctx.query;
            const state = JSON.stringify(ctx.query);
            const redirectUrl = OneCampus.getRedirectUrl(ctx);
            const authPage = OneCampus.auth_service;
            const args = [
                `client_id=${OneCampus.client_id}`,
                `response_type=code`,
                `redirect_uri=${encodeURIComponent(redirectUrl)}`,
                `lang=zh-tw`,
                `scope=${[...OneCampus.Scopes, scope].join(',')}`,
                `state=${encodeURIComponent(state)}`
            ].join('&');
            const authurl = `${authPage}?${args}`;

            ctx.redirect(authurl);
        } catch (error) {
            ctx.status = 500;
            ctx.body = error.message;
        }
    }

    @Service({ middleware: OneCampus.saveCredential })
    public async callback(ctx: Context) {

        const { code, state } = ctx.query;
        const redirectUrl = OneCampus.getRedirectUrl(ctx);
        const tokenPage = OneCampus.token_service;
        const args = [
            'grant_type=authorization_code',
            `client_id=${OneCampus.client_id}`,
            `client_secret=${OneCampus.client_secret}`,
            `redirect_uri=${encodeURIComponent(redirectUrl)}`,
            `code=${code}`
        ].join('&');
        const tokenUrl = `${tokenPage}?${args}`;

        try {
            const tokenRsp = await got(tokenUrl, { json: true });
            const { access_token } = tokenRsp.body;

            const meUrl = `${OneCampus.me_service}?access_token=${access_token}`;
            const meRsp = await got(meUrl, { json: true });
            ctx.body = { state: JSON.parse(state), token: tokenRsp.body, me: meRsp.body };
        } catch (error) {
            ctx.status = 500;
            ctx.body = error.message;
        }
    }

    @Service()
    public async renewAccessToken(ctx: Context) {
        const { id: savior_id } = ctx.session!.user;
        const { id } = ctx.query as any;
        const db = ctx.db;

        try {
            const cmd = 'select content from credential where id = $(id) and ref_savior_id = $(savior_id)';
            const creden = await db.oneOrNone(cmd, { id, savior_id });
            const { refresh_token } = creden.content as AuthToken;
    
            const redirectUrl = OneCampus.getRedirectUrl(ctx);
            const args = [
                `client_id=${OneCampus.client_id}`,
                `client_secret=${OneCampus.client_secret}`,
                `redirectUrl=${redirectUrl}`,
                `refresh_token=${refresh_token}`,
                `grant_type=refresh_token`,
                `response_type=token`
            ];
            const renewurl = `${OneCampus.token_service}?${args.join('&')}`;
            const newtoken = await got(renewurl, { json: true });

            const updateToken = `update credential set content=$(content) where id=$(id) and ref_savior_id = $(savior_id)`;
            await db.none(updateToken, { id, savior_id, content: newtoken.body });
            ctx.body = { success: true };

        } catch(error) {
            ctx.status = 500;
            ctx.body = error.message;
        }

    }

    /**
     *儲存取得的 Credential。
     */
    public static async saveCredential(ctx: Context, next: any) {
        await next();

        const {
            state: { kind, usage },
            me: { mail },
            token
        } = ctx.body as CallbackResult;

        try {
            ctx.body = await Credential.saveCredential(ctx, kind, usage, mail, OneCampus.provider, token);
            ctx.redirect(OneCampus.postLoginUrl);
        } catch(error) {
            ctx.status = 500;
            ctx.body = error.message;
        }
    }

    private static getRedirectUrl(ctx: Context) {
        return `${ctx.protocol}://${ctx.host}/api/onecampus/callback`;
    }
}

export interface CallbackResult {
    state: AuthState;
    token: AuthToken;
    me: MeInfo;
}

export interface MeInfo {
    uuid: string;
    firstName: string;
    lastName: string;
    language: string;
    mail: string;
}

export interface AuthToken {
    access_token: string;
    expires_in: number;
    token_type: string;
    scope: string;
    refresh_token: string;
    static?: boolean;
}

export interface AuthState {
    kind: string;
    usage: string;
    scope: string;
}