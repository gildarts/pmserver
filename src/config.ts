export default {
    "connectionString": "postgres://postgres:12345@localhost:5432/underground",
    "google_secret": {
        "client_id": "58630438457-trem1rv7g6rui1eimednqhps9p7ucoh9.apps.googleusercontent.com",
        "project_id": "emba-5545a",
        "auth_uri": "https://accounts.google.com/o/oauth2/auth",
        "token_uri": "https://www.googleapis.com/oauth2/v3/token",
        "auth_provider_x509_cert_url": "https://www.googleapis.com/oauth2/v1/certs",
        "client_secret": "j1v49F0ep10dfG_IvM2YqLe8",
        "redirect_uris": [
            "urn:ietf:wg:oauth:2.0:oob",
            "http://localhost:4200/api/auth/callback",
            "http://localhost:3000/api/auth/callback",
            "http://localhost:3535/api/auth/callback",
            "http://devg.ischool.com.tw:3000/api/auth/callback",
            "http://devg.ischool.com.tw:3535/api/auth/callback"
        ]
    }
}
