import { connection } from './database';
import moment from 'moment';

export default {
    // refresh_token: 1/7rQbXkPA9h8cHaLJeTtH5QrrhMBHyGU3WCtPzybVzUI
    get: async (key: string, maxAge: number, { rolling }: { rolling: any }) => {
        const cmd = "select * from session where session_id = $(key)";
        const record = await connection.oneOrNone(cmd, { key: key });

        if (record) {
            return record.data;
        } else {
            return null;
        }
    },
    set: async (key: string, sess: any, maxAge: number, { rolling, changed }: { rolling: any, changed: any }) => {
        /*
        WITH src as (
            select '1535030939286-azf6ApkIV5yRpsCiwPrTM0zR4Xs8Hi9m'::character varying as session_id, 12345 as expiry_date, '{}'::JSON as data
        ), update_part as (
            update "session" 
            set expiry_date = src.expiry_date, "data" = src.data 
            from src 
            where "session".session_id = src.session_id
            returning src.*
        ), insert_part as (
            insert into "session"(session_id, expiry_date, data)
            select src.session_id, src.expiry_date, src.data
            from src left join "session" s on src.session_id = s.session_id
            where s.session_id is null
            returning session.*
        )
        select session_id, expiry_date, data from update_part
        union all
        select session_id, expiry_date, data from insert_part
        */

        try {
            if (!changed) return;

            const data = JSON.stringify(sess);
            const time = moment().add(1, "hour").valueOf();

            const get = "select expiry_date from session where session_id = $(key)";
            const record = await connection.oneOrNone(get, { key: key });
            if (record) {
                const cmd = "update session set expiry_date = $(time), data = $(data) where session_id = $(key)";
                await connection.none(cmd, { key: key, time: time, data: data });
            } else {
                const cmd = "insert into session(session_id, expiry_date, data) values($(key), $(time), $(data))";
                await connection.none(cmd, { key: key, time: time, data: data });
            }
        } catch (error) {
            console.error(`set session occure error: ${error.message}`);
        }
    },
    destroy: async (key: string) => {
        const cmd = "delete from session where session_id = $(key)";
        await connection.none(cmd, { key: key });
    }
};