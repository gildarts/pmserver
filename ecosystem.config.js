module.exports = {
  apps: [{
    name: "quasar",
    script: "./dist/main.js",
    watch: true,
    node_args: [
      "--inspect"
    ]
  }, {
    name: "quasar_brk",
    script: "./dist/main.js",
    watch: true,
    node_args: [
      "--inspect-brk"
    ]
  }]
}